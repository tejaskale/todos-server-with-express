const express = require('express')
const todosData=require('./data')
const app = express();
const port = process.env.PORT || 8000 ;


app.use(express.json());



//Get Method to get all Tasks

app.get("/todos/", async (req, res) => {
    
    if(todosData.length === 0){
        res.status(200).json({msg: "no todos available"})

    }else{
        res.status(200).json(todosData);
    }
    
  });



//Get Method to get unique Task

app.get("/todos/:id", async (req, res) => {
    const todoId = req.params.id; 
    const todo = todosData.find(todo=>todo.id === parseInt(todoId))
            if (todo) {
                todosData.push(todo)
                res.status(200).json(todo);
            } else {
                res.status(404).json({msg:`Todos task with given Id ${id} not found`})
            } 
  });


//Post Method

app.post("/todos/", async (req, res) => {
    const todo=req.body;
    const newTodo = {
        id : todosData.length+1,
        ...todo
    }
    todosData.push(newTodo)
  
    res.status(200).json(newTodo);
  });


  //PUT method to update a task

  app.put("/todos/:id", async (req, res) => {
    const todoId = req.params.id;
    const todo=req.body;
    
    const newTask = todosData.find(todo=>todo.id === parseInt(todoId))
    const todoIndex=todosData.find(todo=>todo.id === parseInt(todoId))

    if (newTask) {
        let updated_todo={
            id:todoId,
            task:todo.task,
            completeStatus:todo.completeStatus
        }
        todosData.splice(todoIndex,1,updated_todo)
        res.status(200).json(updated_todo);
    } else {
        res.status(404).json({msg:`Todos task with given Id ${id} not found`})

    }
  });



  //Delete Method to delete a task

  app.delete("/todos/:id", async (req, res) => {
        const todoId = req.params.id;
        const todo = todosData.find(todo=>todo.id === parseInt(todoId))
        const todoIndex=todosData.findIndex(todo=>todo.id === parseInt(todoId))
        
                if (todo) {
                    todosData.splice(todoIndex,1)
                    res.status(200).json({msg:`Todos task with id ${todoId} deleted successfully`})
                } else {
                    res.status(404).json({msg:`Todos task with given Id ${id} not found`});

                }
  });

app.listen(port, () => {
  console.log("app listening on port http://localhost:8000")
})
